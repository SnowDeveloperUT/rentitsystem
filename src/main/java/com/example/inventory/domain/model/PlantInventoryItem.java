package com.example.inventory.domain.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class PlantInventoryItem {
    @EmbeddedId
    PlantInventoryItemID id;

    String serialNumber;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="plant_info_id"))})
    PlantInventoryEntryID plantInfo;

    @Enumerated(EnumType.STRING)
    EquipmentCondition condition;
}
