package com.example.inventory.domain.model;

import com.example.common.domain.model.BusinessPeriod;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
@AllArgsConstructor(staticName = "of")
public class PlantReservation {
    @EmbeddedId
    PlantReservationID id;

    @Embedded
    BusinessPeriod schedule;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="plant_id"))})
    PlantInventoryItemID plant;
}
