package com.example.inventory.application.dto;



import com.example.common.application.dto.BusinessPeriodDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.ResourceSupport;


@Getter
@Setter
@NoArgsConstructor
public class PlantReservationDTO extends ResourceSupport {
    Long _id;
    BusinessPeriodDTO schedule;
    PlantInventoryItemDTO plant;
}
