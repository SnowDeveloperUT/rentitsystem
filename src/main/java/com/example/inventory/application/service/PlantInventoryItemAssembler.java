package com.example.inventory.application.service;


import com.example.inventory.application.dto.PlantInventoryItemDTO;
import com.example.inventory.domain.model.PlantInventoryItem;
import com.example.inventory.domain.model.PlantInventoryItemID;
import com.example.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.inventory.rest.controller.PlantInventoryItemRestController;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantInventoryItemAssembler extends ResourceAssemblerSupport<PlantInventoryItem, PlantInventoryItemDTO> {
    @Autowired
    PlantInventoryEntryAssembler entryAssembler;
    @Autowired
    PlantInventoryItemRepository itemRepository;

    public PlantInventoryItemAssembler() {
        super(PlantInventoryItemRestController.class, PlantInventoryItemDTO.class);
    }

    @Override
    public PlantInventoryItemDTO toResource(PlantInventoryItem item) {
        PlantInventoryItemDTO dto = createResourceWithId(item.getId().getId(), item);
        dto.set_id(item.getId().getId());
        dto.setCondition(item.getCondition());
        dto.setPlantInfo(entryAssembler.toResource(item.getPlantInfo()));
        dto.setSerialNumber(item.getSerialNumber());
        return dto;
    }

    public PlantInventoryItemDTO toResource(PlantInventoryItemID plant) {
        return toResource(itemRepository.findOne(plant));
    }
}
