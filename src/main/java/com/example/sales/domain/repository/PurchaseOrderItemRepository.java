package com.example.sales.domain.repository;

import com.example.sales.domain.model.PurchaseOrderItem;
import com.example.sales.domain.model.PurchaseOrderItemID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by snowwhite on 6/22/2017.
 */
@Repository
public interface PurchaseOrderItemRepository extends JpaRepository<PurchaseOrderItem, PurchaseOrderItemID> {

}
