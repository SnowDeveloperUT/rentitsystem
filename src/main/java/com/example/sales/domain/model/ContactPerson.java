package com.example.sales.domain.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

/**
 * Created by snowwhite on 6/22/2017.
 */
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class ContactPerson {
	@EmbeddedId
	ContactPersonID id;

	String name;
	String email;

	public static ContactPerson of(ContactPersonID id, String name, String email) {
		ContactPerson contactPerson = new ContactPerson();
		contactPerson.id = id;
		contactPerson.name = name;
		contactPerson.email = email;
		return contactPerson;
	}
}
