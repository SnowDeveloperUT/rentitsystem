package com.example.sales.domain.model;

public enum POStatus {
    PENDING, OPEN, REJECTED, CLOSED, PENDING_RESERVATIONS, PENDING_CONFIRMATION, AVAILABLE, UNAVAILABLE
}
