package com.example.sales.domain.model;

import com.example.common.domain.model.BusinessPeriod;
import com.example.inventory.domain.model.PlantInventoryEntry;
import com.example.inventory.domain.model.PlantReservation;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class PurchaseOrder {
    @EmbeddedId
    PurchaseOrderID id;

    @ElementCollection
    @AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="purchase_order_item_id"))})
    List<PurchaseOrderItemID> items = new ArrayList<>();

    @Column(precision = 8, scale = 2)
    BigDecimal total;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="person_id"))})
    ContactPersonID person;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="customer_id"))})
    CustomerID customer;

    String address;

    @Enumerated(EnumType.STRING)
    POStatus status;

    public static PurchaseOrder of(PurchaseOrderID id, ContactPerson contactPerson, Customer customer, String address) {
        PurchaseOrder po = new PurchaseOrder();
        po.id = id;
        po.person = contactPerson.getId();
        po.customer = customer.getId();
        po.address = address;
        return po;
    }
}
