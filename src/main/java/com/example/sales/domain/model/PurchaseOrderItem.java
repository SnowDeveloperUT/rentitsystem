package com.example.sales.domain.model;

import com.example.common.domain.model.BusinessPeriod;
import com.example.inventory.domain.model.PlantInventoryEntryID;
import com.example.inventory.domain.model.PlantReservationID;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by snowwhite on 6/22/2017.
 */
@Entity
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class PurchaseOrderItem {
	@EmbeddedId
	PurchaseOrderItemID id;

	@Embedded
	@AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="plant_id"))})
	PlantInventoryEntryID plant;

	@Embedded
	@AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="purchase_order_id"))})
	PurchaseOrderID purchaseOrderID;

	@Embedded
	BusinessPeriod rentalPeriod;

	@ElementCollection
	@AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="reservation_id"))})
	List<PlantReservationID> reservations = new ArrayList<>();

	LocalDate issueDate;
	LocalDate paymentSchedule;

	@Column(precision = 8, scale = 2)
	BigDecimal total;

	@Enumerated(EnumType.STRING)
	POStatus status;

	public static PurchaseOrderItem of(PurchaseOrderItemID id, PlantInventoryEntryID plant, BusinessPeriod period, PurchaseOrderID purchaseOrderID) {
		PurchaseOrderItem po = new PurchaseOrderItem();
		po.id = id;
		po.plant = plant;
		po.rentalPeriod = period;
		po.status = POStatus.PENDING;
		po.purchaseOrderID = purchaseOrderID;
		return po;
	}

	public void confirmReservation(PlantReservationID reservation, BigDecimal plantPrice) {
		reservations.add(reservation);
		total = plantPrice.multiply(BigDecimal.valueOf(rentalPeriod.numberOfWorkingDays()));
		status = POStatus.OPEN;
	}

	public void accept() {
		if (status.equals(POStatus.PENDING))
			status = POStatus.OPEN;
	}

	public void reject() {
		if (status.equals(POStatus.PENDING))
			status = POStatus.REJECTED;
	}

	public void close() {
		if (status.equals(POStatus.OPEN))
			status = POStatus.CLOSED;
	}
}
