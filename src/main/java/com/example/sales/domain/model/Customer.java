package com.example.sales.domain.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by snowwhite on 6/22/2017.
 */
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class Customer {
	@EmbeddedId
	CustomerID id;

	String name;
	String email;

	@Column(precision = 8, scale = 2)
	BigDecimal credit;

	@Embedded
	@AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="contact_person_id"))})
	List<ContactPersonID> contacts;

	public static Customer of(CustomerID id, String name, String email, BigDecimal credit) {
		Customer customer = new Customer();
		customer.id = id;
		customer.name = name;
		customer.email = email;
		customer.credit = credit;
		return customer;
	}
}
