package com.example.sales.domain.validation;

import com.example.common.domain.model.BusinessPeriod;
import com.example.common.domain.validation.BusinessPeriodValidator;
import com.example.sales.domain.model.ContactPerson;
import com.example.sales.domain.model.POStatus;
import com.example.sales.domain.model.PurchaseOrderItem;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by snowwhite on 6/22/2017.
 */
public class PurchaseOrderItemValidator implements Validator {
	private final BusinessPeriodValidator periodValidator;

	public PurchaseOrderItemValidator(BusinessPeriodValidator periodValidator,
	                                  ContactPersonValidator contactValidator) {
		if (periodValidator == null) {
			throw new IllegalArgumentException("The supplied [Validator] is " +
					"required and must not be null.");
		}
		if (!periodValidator.supports(BusinessPeriod.class)) {
			throw new IllegalArgumentException("The supplied [Validator] must " +
					"support the validation of [BusinessPeriod] instances.");
		}
		if (contactValidator == null) {
			throw new IllegalArgumentException("The supplied [Validator] is " +
					"required and must not be null.");
		}
		if (!contactValidator.supports(ContactPerson.class)) {
			throw new IllegalArgumentException("The supplied [Validator] must " +
					"support the validation of [ContactPerson] instances.");
		}

		this.periodValidator = periodValidator;
	}

	@Override
	public boolean supports(Class<?> aClass) {
		return PurchaseOrderItem.class.isAssignableFrom(aClass);
	}

	@Override
	public void validate(Object o, Errors errors) {
		PurchaseOrderItem item = (PurchaseOrderItem)o;
		if (item.getId() == null)
			errors.reject("id", "Purchase Order id cannot be null");
		if (item.getPlant() == null)
			errors.reject("plant", "Reference to 'PlantInventoryEntry' cannot be null");

		if (!item.getStatus().equals(POStatus.PENDING)) {
			if (item.getReservations().size() == 0)
				errors.reject("reservations", "At least one reference to 'PlantReservation' is required");
			if (item.getTotal() == null)
				errors.rejectValue("total", "Purchase order's total cannot be null");
			else if (item.getTotal().signum() != 1)
				errors.rejectValue("total", "Purchase order's total must be a positive value");
		}

		errors.pushNestedPath("rentalPeriod");
		ValidationUtils.invokeValidator(periodValidator, item.getRentalPeriod(), errors);
		errors.popNestedPath();
	}
}
