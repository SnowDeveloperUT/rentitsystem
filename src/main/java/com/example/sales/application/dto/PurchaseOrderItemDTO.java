package com.example.sales.application.dto;

import com.example.common.application.dto.BusinessPeriodDTO;
import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.sales.domain.model.POStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/22/2017.
 */
@Data
@NoArgsConstructor(force = true)
public class PurchaseOrderItemDTO {
	Long _id;
	PurchaseOrderDTO purchaseOrder;
	PlantInventoryEntryDTO plant;
	BusinessPeriodDTO rentalPeriod;
	BigDecimal total;
	POStatus status;
}
