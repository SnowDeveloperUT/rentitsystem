package com.example.sales.application.dto;

import com.example.common.rest.ResourceSupport;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by snowwhite on 6/22/2017.
 */
@Data
@NoArgsConstructor(force = true)
public class ContactPersonDTO extends ResourceSupport {
	Long _id;
	String name;
	String email;
	CustomerDTO customer;
}
