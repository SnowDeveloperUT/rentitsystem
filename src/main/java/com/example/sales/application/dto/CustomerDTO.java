package com.example.sales.application.dto;

import com.example.common.rest.ResourceSupport;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/22/2017.
 */
@Data
@NoArgsConstructor(force = true)
public class CustomerDTO extends ResourceSupport {
	Long _id;
	String name;
	String email;
	BigDecimal credit;
}
