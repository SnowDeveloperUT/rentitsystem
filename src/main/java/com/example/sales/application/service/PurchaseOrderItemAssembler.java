package com.example.sales.application.service;

import com.example.common.application.dto.BusinessPeriodDTO;
import com.example.common.application.exceptions.PlantNotFoundException;
import com.example.common.rest.ExtendedLink;
import com.example.inventory.application.service.InventoryService;
import com.example.sales.application.dto.PurchaseOrderItemDTO;
import com.example.sales.domain.model.PurchaseOrderItem;
import com.example.sales.rest.controller.PurchaseOrderRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.PATCH;

/**
 * Created by snowwhite on 6/22/2017.
 */
@Service
public class PurchaseOrderItemAssembler extends ResourceAssemblerSupport<PurchaseOrderItem, PurchaseOrderItemDTO> {
	@Autowired
	InventoryService inventoryService;

	public PurchaseOrderItemAssembler() {
		super(PurchaseOrderRestController.class, PurchaseOrderItemDTO.class);
	}

	@Override
	public PurchaseOrderItemDTO toResource(PurchaseOrderItem purchaseOrder) {
		PurchaseOrderItemDTO dto = createResourceWithId(purchaseOrder.getId().getId(), purchaseOrder);
		try {
			dto.setPlant(inventoryService.findPlant(purchaseOrder.getPlant()));
		} catch (PlantNotFoundException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
		dto.set_id(purchaseOrder.getId().getId());
		dto.setRentalPeriod(BusinessPeriodDTO.of(purchaseOrder.getRentalPeriod().getStartDate(), purchaseOrder.getRentalPeriod().getEndDate()));
		dto.setTotal(purchaseOrder.getTotal());
		dto.setStatus(purchaseOrder.getStatus());

		try {
			switch (purchaseOrder.getStatus()) {
				case PENDING:
					dto.add(new ExtendedLink(
							linkTo(methodOn(PurchaseOrderRestController.class).acceptPurchaseOrder(dto.get_id())).toString(),
							"accept", PATCH));
					dto.add(new ExtendedLink(
							linkTo(methodOn(PurchaseOrderRestController.class).rejectPurchaseOrder(dto.get_id())).toString(),
							"reject", DELETE));
					break;
				case OPEN:
					dto.add(new ExtendedLink(
							linkTo(methodOn(PurchaseOrderRestController.class).closePurchaseOrder(dto.get_id())).toString(),
							"close", DELETE));
					break;
			}
		} catch (Exception e) {}
		return dto;
	}


}
