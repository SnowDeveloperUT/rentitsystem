package com.example.sales.application.service;

import com.example.inventory.application.service.InventoryService;
import com.example.sales.application.dto.CustomerDTO;
import com.example.sales.domain.model.Customer;
import com.example.sales.rest.controller.PurchaseOrderRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by snowwhite on 6/22/2017.
 */
@Service
public class CustomerAssembler extends ResourceAssemblerSupport<Customer, CustomerDTO> {
	@Autowired
	InventoryService inventoryService;

	public CustomerAssembler() {
		super(PurchaseOrderRestController.class, CustomerDTO.class);
	}

	@Override
	public CustomerDTO toResource(Customer customer) {
		CustomerDTO dto = createResourceWithId(customer.getId().getId(), customer);
		dto.setName(customer.getName());
		dto.setEmail(customer.getEmail());
		dto.setCredit(customer.getCredit());
		return dto;
	}

}
