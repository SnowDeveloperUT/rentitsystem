package com.example.sales.application.service;

import com.example.inventory.application.service.InventoryService;
import com.example.sales.application.dto.ContactPersonDTO;
import com.example.sales.domain.model.ContactPerson;
import com.example.sales.rest.controller.PurchaseOrderRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

/**
 * Created by snowwhite on 6/22/2017.
 */
@Service
public class ContactPersonAssembler extends ResourceAssemblerSupport<ContactPerson, ContactPersonDTO> {
	@Autowired
	InventoryService inventoryService;

	public ContactPersonAssembler() {
		super(PurchaseOrderRestController.class, ContactPersonDTO.class);
	}

	@Override
	public ContactPersonDTO toResource(ContactPerson contactPerson) {
		ContactPersonDTO dto = createResourceWithId(contactPerson.getId().getId(), contactPerson);
		dto.setName(contactPerson.getName());
		dto.setEmail(contactPerson.getEmail());
		return dto;
	}
}
