package com.example.sales.application.service;

import com.example.common.application.exceptions.PlantNotFoundException;
import com.example.common.domain.model.BusinessPeriod;
import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.inventory.application.service.InventoryService;
import com.example.inventory.domain.model.PlantInventoryEntryID;
import com.example.sales.application.dto.PurchaseOrderDTO;
import com.example.sales.application.dto.PurchaseOrderItemDTO;
import com.example.sales.domain.model.*;
import com.example.sales.domain.repository.ContactPersonRepository;
import com.example.sales.domain.repository.CustomerRepository;
import com.example.sales.domain.repository.PurchaseOrderItemRepository;
import com.example.sales.domain.repository.PurchaseOrderRepository;
import com.example.sales.infrastructure.SalesIdentifierGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class SalesService {
    @Autowired
    InventoryService inventoryService;
    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;
    @Autowired
    PurchaseOrderItemAssembler purchaseOrderItemAssembler;
    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    PurchaseOrderItemRepository purchaseOrderItemRepository;

    @Autowired
    SalesIdentifierGenerator identifierGenerator;

    @Autowired
    ContactPersonRepository contactPersonRepository;
    @Autowired
    CustomerRepository customerRepository;

    public PurchaseOrderDTO createPurchaseOrder(PurchaseOrderDTO purchaseOrderDTO) throws BindException, PlantNotFoundException {

        ContactPerson contactPerson = ContactPerson.of(identifierGenerator.nextContactPersonID(), purchaseOrderDTO.getContactPerson().getName(), purchaseOrderDTO.getContactPerson().getEmail());
        Customer customer = Customer.of(identifierGenerator.nextCustomerID(), purchaseOrderDTO.getCustomer().getName(), purchaseOrderDTO.getCustomer().getEmail(), purchaseOrderDTO.getCustomer().getCredit());

        contactPersonRepository.save(contactPerson);
        customerRepository.save(customer);

        PurchaseOrder po = PurchaseOrder.of(
                identifierGenerator.nextPurchaseOrderID(),
                contactPerson,
                customer,
                purchaseOrderDTO.getAddress()
        );

        purchaseOrderRepository.save(po);

        List<PurchaseOrderItemDTO> items = purchaseOrderDTO.getItems();
        List<PurchaseOrderItem> poItems = new ArrayList<>();
        BigDecimal totalCost = new BigDecimal(0);
        for(PurchaseOrderItemDTO item : items){
            poItems.add(createPurchaseOrderItem(item, po.getId(), purchaseOrderDTO.getCustomer().getCredit()));
        }
        boolean isReserve = false;
        int count = 0;
        for(PurchaseOrderItem item : poItems){
            if(item.getStatus() == POStatus.OPEN){
                count++;
                BigDecimal sum = new BigDecimal(totalCost.doubleValue() + item.getTotal().doubleValue());
                totalCost = sum;
                if(!false)
                    po.setStatus(POStatus.PENDING_CONFIRMATION);
            }
            else{
                po.setStatus(POStatus.PENDING_RESERVATIONS);
                isReserve = true;
            }
        }
        if(count == poItems.size()){
            po.setStatus(POStatus.OPEN);
        }

        po.setTotal(totalCost);
        purchaseOrderRepository.save(po);

        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderItem createPurchaseOrderItem(PurchaseOrderItemDTO purchaseOrderItemDTO, PurchaseOrderID poId, BigDecimal credit) throws BindException, PlantNotFoundException {
        PlantInventoryEntryDTO plant = inventoryService.findPlantFullRepresentation(purchaseOrderItemDTO.getPlant());
        PurchaseOrderItem poItem = PurchaseOrderItem.of(
                identifierGenerator.nextPurchaseOrderItemID(),
                PlantInventoryEntryID.of(plant.get_id()),
                BusinessPeriod.of(purchaseOrderItemDTO.getRentalPeriod().getStartDate(), purchaseOrderItemDTO.getRentalPeriod().getEndDate()),
                poId
        );

        BigDecimal cost = new BigDecimal(plant.getPrice().doubleValue()*poItem.getRentalPeriod().numberOfWorkingDays());
        poItem.setTotal(cost);
        if(credit.doubleValue() > cost.doubleValue()){
            poItem.setStatus(POStatus.OPEN);
        }
        //        DataBinder binder = new DataBinder(poItem);
        //        binder.addValidators(new PurchaseOrderItemValidator(new BusinessPeriodValidator(), new ContactPersonValidator()));
        //        binder.validate();
        //
        //        if (binder.getBindingResult().hasErrors())
        //            throw new BindException(binder.getBindingResult());

        purchaseOrderItemRepository.save(poItem);

        return poItem;
    }

    public PurchaseOrderDTO findPurchaseOrder(PurchaseOrderID id) {
        return purchaseOrderAssembler.toResource(purchaseOrderRepository.findOne(id));
    }

    public List<PurchaseOrderDTO> findAll() {
        return purchaseOrderAssembler.toResources(purchaseOrderRepository.findAll());
    }

    public PurchaseOrderDTO updatePO(PurchaseOrderID purchaseOrderID){
        PurchaseOrder po = purchaseOrderRepository.getOne(purchaseOrderID);

        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderItemDTO acceptPurchaseOrder(PurchaseOrderItemID id) {
        PurchaseOrderItem poItem = purchaseOrderItemRepository.findOne(id);
        poItem.accept();
        purchaseOrderItemRepository.save(poItem);

        return purchaseOrderItemAssembler.toResource(poItem);
    }

    public PurchaseOrderItemDTO rejectPurchaseOrder(PurchaseOrderItemID id) {
        PurchaseOrderItem poItem = purchaseOrderItemRepository.findOne(id);
        poItem.reject();
        purchaseOrderItemRepository.save(poItem);
        return purchaseOrderItemAssembler.toResource(poItem);
    }

    public PurchaseOrderItemDTO closePurchaseOrder(PurchaseOrderItemID id) {
        PurchaseOrderItem poItem = purchaseOrderItemRepository.findOne(id);
        poItem.close();
        purchaseOrderItemRepository.save(poItem);
        return purchaseOrderItemAssembler.toResource(poItem);
    }
}
