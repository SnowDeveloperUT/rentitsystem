package com.example.sales.application.service;

import com.example.inventory.application.service.InventoryService;
import com.example.sales.application.dto.PurchaseOrderDTO;
import com.example.sales.domain.model.ContactPerson;
import com.example.sales.domain.model.PurchaseOrder;
import com.example.sales.domain.repository.ContactPersonRepository;
import com.example.sales.domain.repository.CustomerRepository;
import com.example.sales.rest.controller.PurchaseOrderRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@Service
public class PurchaseOrderAssembler extends ResourceAssemblerSupport<PurchaseOrder, PurchaseOrderDTO> {
    @Autowired
    InventoryService inventoryService;
    @Autowired
    ContactPersonAssembler contactPersonAssembler;
    @Autowired
    CustomerAssembler customerAssembler;
    @Autowired
    PurchaseOrderItemAssembler purchaseOrderItemAssembler;
    @Autowired
    ContactPersonRepository contactPersonRepository;
    @Autowired
    CustomerRepository customerRepository;


    public PurchaseOrderAssembler() {
        super(PurchaseOrderRestController.class, PurchaseOrderDTO.class);
    }

    @Override
    public PurchaseOrderDTO toResource(PurchaseOrder purchaseOrder) {
        PurchaseOrderDTO dto = createResourceWithId(purchaseOrder.getId().getId(), purchaseOrder);
        dto.setTotal(purchaseOrder.getTotal());
        ContactPerson contactPerson = contactPersonRepository.getOne(purchaseOrder.getPerson());
        dto.setContactPerson(contactPersonAssembler.toResource(contactPerson));
        dto.setCustomer(customerAssembler.toResource(customerRepository.getOne(purchaseOrder.getCustomer())));

        return dto;
    }
}
