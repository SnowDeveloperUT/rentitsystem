package com.example.sales.infrastructure;

import com.example.sales.domain.model.ContactPersonID;
import com.example.sales.domain.model.CustomerID;
import com.example.sales.domain.model.PurchaseOrderID;
import com.example.sales.domain.model.PurchaseOrderItemID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.common.infrastructure.HibernateBasedIdentifierGenerator;

/**
 * Created by snowwhite on 6/22/2017.
 */
@Service
public class SalesIdentifierGenerator {
	@Autowired
	HibernateBasedIdentifierGenerator hibernateGenerator;

	public PurchaseOrderID nextPurchaseOrderID() {
		return PurchaseOrderID.of(hibernateGenerator.getID("PurchaseOrderIDSequence"));
	}

	public PurchaseOrderItemID nextPurchaseOrderItemID() {
		return PurchaseOrderItemID.of(hibernateGenerator.getID("PurchaseOrderItemIDSequence"));
	}

	public ContactPersonID nextContactPersonID() {
		return ContactPersonID.of(hibernateGenerator.getID("ContactPersonIDSequence"));
	}

	public CustomerID nextCustomerID() {
		return CustomerID.of(hibernateGenerator.getID("CustomerIDSequence"));
	}
}
