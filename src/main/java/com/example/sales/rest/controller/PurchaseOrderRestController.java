package com.example.sales.rest.controller;

import com.example.common.application.exceptions.PlantNotFoundException;
import com.example.sales.application.dto.PurchaseOrderDTO;
import com.example.sales.application.dto.PurchaseOrderItemDTO;
import com.example.sales.application.service.SalesService;
import com.example.sales.domain.model.PurchaseOrderID;
import com.example.sales.domain.model.PurchaseOrderItemID;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.*;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;

/**
 * Created by snowwhite on 6/22/2017.
 */

@RestController
@RequestMapping("/api/sales/orders")
@CrossOrigin
public class PurchaseOrderRestController {
	@Autowired
	SalesService salesService;
	@Autowired @Qualifier("_halObjectMapper")
	ObjectMapper mapper;

	@RequestMapping(method = POST, path = "")
	public ResponseEntity<PurchaseOrderDTO> createPurchaseOrder(@RequestBody PurchaseOrderDTO poDTO) throws Exception {
		poDTO = salesService.createPurchaseOrder(poDTO);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(new URI(poDTO.getId().getHref()));
		return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED);
	}

	@RequestMapping(method = GET, path = "")
	public List<PurchaseOrderDTO> getAllPurchaseOrders() throws Exception {
		return salesService.findAll();
	}

	@RequestMapping(method = GET, path = "/{id}")
	public PurchaseOrderDTO showPurchaseOrder(@PathVariable Long id) throws Exception {
		PurchaseOrderDTO poDTO = salesService.findPurchaseOrder(PurchaseOrderID.of(id));
		return poDTO;
	}

	@RequestMapping(method = PATCH, path = "/{id}/accept")
	public PurchaseOrderItemDTO acceptPurchaseOrder(@PathVariable Long id) throws Exception {
		return salesService.acceptPurchaseOrder(PurchaseOrderItemID.of(id));
	}

	@RequestMapping(method = DELETE, path = "/{id}/accept")
	public PurchaseOrderItemDTO rejectPurchaseOrder(@PathVariable Long id) throws Exception {
		return salesService.rejectPurchaseOrder(PurchaseOrderItemID.of(id));
	}

	@RequestMapping(method = DELETE, path = "/{id}")
	public PurchaseOrderItemDTO closePurchaseOrder(@PathVariable Long id) throws Exception {
		return salesService.closePurchaseOrder(PurchaseOrderItemID.of(id));
	}

	@ExceptionHandler({BindException.class, PlantNotFoundException.class})
	@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
	public String bindExceptionHandler(Exception ex) {
		return ex.getMessage();
	}
}
